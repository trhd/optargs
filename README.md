OPTARGS
=======

WHAT IS OPTARGS
---------------

Optargs is an option and argument management library with the goal of
being easy-to-use and able of providing visually pleasant, informative and
useful help texts for command line applications with as little input from
the developer as possible.


STATUS
------

Development of optargs has (at least for now) moved to
[Sourcehut](https://sr.ht/~trhd/optargs).
